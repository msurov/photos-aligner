Description
-----------
The simple script for aligning and cropping photos. Helpful for cropping photos of documents:
![source image](doc/im.jpg)
![cropped image](doc/im-cropped.jpg)

Running
-------
```
	python im_crop_rect.py doc/im.jpg
```
