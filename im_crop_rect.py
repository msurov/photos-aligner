import cv2
import matplotlib.pyplot as plt
import numpy as np
import argparse
from os.path import splitext


class PickCorners:

    def __init__(self, img, N=-1):
        self.corners = []
        self.ax = plt.gca()
        self.fig = plt.gcf()
        self.imrect = img.shape[0:2]
        self.polyline, = self.ax.plot([], [], '-o')
        self.N = N
        _img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        self.ax.imshow(_img)
        self.fig.canvas.mpl_connect('button_press_event',
            lambda event: self.onclick(event))
        self.fig.canvas.mpl_connect('key_press_event', 
            lambda event: self.onkey(event))
        plt.show()


    def __redraw(self):
        if len(self.corners) == 0:
            x = []
            y = []
        else:
            x,y = zip(*self.corners)
            x = list(x)
            y = list(y)

        if len(x) == self.N:
            print x
            x += [x[0]]
            y += [y[0]]

        self.polyline.set_data(x, y)
        self.fig.canvas.draw()


    def __add_corner(self, corner):
        if self.corners == self.N:
            return
        self.corners += [corner]
        self.__redraw()


    def __rem_corner(self):
        self.corners = self.corners[:-1]
        self.__redraw()


    def onkey(self, event):
        if event.key == 'escape':
            self.__rem_corner()
        elif event.key == 'enter':
            plt.close()
        elif event.key == 'backspace':
            print event.key


    def onclick(self, event):
        if event.button != 1:
            return

        if event.dblclick is False:
            return

        corner = (event.xdata, event.ydata)
        # TODO: valid?

        self.__add_corner(corner)


def main(srcname, dstname):
    im = cv2.imread(srcname)
    p = PickCorners(im, 4)
    if len(p.corners) != 4:
        print p.corners
        return None

    # TODO: clockwise?
    # TODO: contour beginning?

    srcpts = np.array(p.corners, dtype=np.float32)
    x,y = zip(*srcpts)
    x1 = np.min(x)
    x2 = np.max(x)
    y1 = np.min(y)
    y2 = np.max(y)
    w = x2 - x1
    h = y2 - y1
    dstpts = np.array([
        [0,0],
        [w,0],
        [w,h],
        [0,h]
    ], dtype=np.float32)
    H,_ = cv2.findHomography(srcpts, dstpts)
    cropped = cv2.warpPerspective(im, H, (w,h), flags=cv2.INTER_CUBIC)
    cv2.imwrite(dstname, cropped)


def add_suffix(path, suf):
    p,e = splitext(path)
    return p + suf + e


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('srcim', type=str, help='path to an image to crop', nargs=1)
    args = parser.parse_args()
    srcim = args.srcim[0]
    dstim = add_suffix(srcim, '-cropped')
    main(srcim, dstim)
